#ifndef __BF_H__
#define __BF_H__

typedef enum _BF_RC
{
	BF_RC_SUCCESS = 0,						/*!< Function completed successfully. */
	BF_RC_INVALID_PARAMETER,				/*!< One of the parameters suppied to the function is invalid (null pointer). */
	BF_RC_INVALID_KEY,						/*!< The length of the key supplied to the #BF_Init/#BF_Reset function is either greater than #BF_MAX_KEY_LENGTH or less than #BF_MIN_KEY_LENGTH. */
	BF_RC_WEAK_KEY,							/*!< The key supplied to the #BF_Init/#BF_Reset function has been deemed to be weak, and should not be used. */
	BF_RC_BAD_BUFFER_LENGTH,				/*!< The size of the buffer supplied to one of the encipher/decipher buffer/stream functions is not a multiple of 8. */
	BF_RC_INVALID_MODE,						/*!< The mode specified to the #BF_Init/#BF_Reset function is not supported. */
	BF_RC_TEST_FAILED,						/*!< Self test failed. For more information see stdout (only used by test applications). */
	BF_RC_ERROR,							/*!< Generic error (only used by test applications). */
} BF_RC;

/* Various static array/buffer lengths (do not modify!). */

#define BF_SUBKEYS					18			/*!< Number of subkeys in the P-Array. */
#define BF_SBOXES					4			/*!< Number of S-Boxes. */
#define BF_SBOX_ENTRIES				256			/*!< Number of entries in each S-Box. */

#define BF_MIN_KEY_LENGTH			4			/*!< Maximum length of a key (4-bytes, or 32-bits). */
#define BF_MAX_KEY_LENGTH			56			/*!< Maximum length of a key (56-bytes, or 448-bits). */

/** Blowfish context record. */

typedef struct
{
	unsigned int	pArray [ BF_SUBKEYS ];						/*!< Original P-Array which has been XOR'd with the key, and overwritten with output from #BF_Encipher. */
	unsigned int	sBox [ BF_SBOXES ] [ BF_SBOX_ENTRIES ];		/*!< Original S-Boxes which have been overwritten with output from #BF_Encipher. */
	unsigned int	orgHigh32;									/*!< Original high 32-bytes of the initialisation vector. */
	unsigned int	orgLow32;									/*!< Original low 32-bytes of the initialisation vector. */
	unsigned int	high32;										/*!< Current high 32-bytes of the initialisation vector (used for stream operations). */
	unsigned int	low32;										/*!< Current low 32-bytes of the initialisation vector (used for stream operations). */
} BF_Context;

/* Function prototypes. */

/**
	Initialise a Blowfish context record.

	@param context		Pointer to a Blowfish context record to initialise.
	@param key			Pointer to a key to use for enciphering/deciphering data.
	@param len			Length of the key, which cannot exceed #BF_MAX_KEY_LENGTH bytes (sizeof(#BF_Context::pArray)), or be less than #BF_MIN_KEY_LENGTH bytes.
	@param Mode			Mode to use when enciphering/decipering blocks. For supported modes see #BF_MODE
	@param high32		High 32-bits of the initialisation vector. Required if the Mode parameter is not #BF_MODE_ECB.
	@param low32		Low 32-bits of the initialisation vector. Required if the Mode parameter is not #BF_MODE_ECB.

	@remarks For stream based enciphering/deciphering, call #BF_BeginStream/#BF_EndStream before/after processing the stream.
	@return #BF_RC_SUCCESS				Initialised context record successfully.
	@return #BF_RC_INVALID_PARAMETER	Either the context record or key pointer is null.
	@return #BF_RC_INVALID_KEY			The key is either too short or too long.
	@return #BF_RC_WEAK_KEY				The key has been deemed to be weak.
	@return #BF_RC_INVALID_MODE			The specified mode is not supported.
*/
BF_RC BF_Init ( BF_Context* context, const unsigned char *  key, unsigned long len, unsigned int high32, unsigned int low32 );

/**
	Reinitialise either the key and/or mode and initialisation vector in a Blowfish context record.
	@param context		Pointer to an initialised Blowfish context record to reinitialise.
	@param key			Pointer to a new key to use for enciphering/deciphering data. (May be null to re-use the current key).
	@param len			Length of the new key, which cannot exceed #BF_MAX_KEY_LENGTH bytes (sizeof(#BF_Context::pArray)), or be less than #BF_MIN_KEY_LENGTH bytes. (May be 0 if key is null).
	@param high32		High 32-bits of the new initialisation vector. Required if the Mode parameter is not #BF_MODE_ECB or #BF_MODE_CURRENT.
	@param low32		Low 32-bits of the new initialisation vector. Required if the Mode parameter is not #BF_MODE_ECB or #BF_MODE_CURRENT.

	@remarks See #BF_Init remarks.
	@return #BF_RC_SUCCESS				Reinitialised the context record successfully.
	@return #BF_RC_INVALID_PARAMETER	The context record pointer is null.
	@return #BF_RC_INVALID_KEY			The key is either too short or too long.
	@return #BF_RC_WEAK_KEY				The key has been deemed to be weak.
	@return #BF_RC_INVALID_MODE			The specified mode is not supported.
*/
BF_RC BF_Reset ( BF_Context* context, const unsigned char *  key, unsigned long len, unsigned int high32, unsigned int low32 );

/**
	Clear a blowfish context record.
	@param context	Pointer to an initialised context record to overwrite.
	@remarks Call this function regardless of whether #BF_Init succeeds.
	@remarks It is a security risk to not call this function once you have finished enciphering/deciphering data!
	@remarks The context record may not be used again after this call without first calling #BF_Init.

	@return #BF_RC_SUCCESS				The context record was overwritten successfully.
	@return #BF_RC_INVALID_PARAMETER	The supplied context record pointer is null.
*/
BF_RC BF_Exit ( BF_Context* context );

/**
	Initialise a context record for stream based enciphering/deciphering.
	@param context	Pointer to an initialised context record.
	@remarks Before re-using the context record to process another stream, be sure to end and begin a new stream using #BF_EndStream and BF_BeginStream.
	@remarks After calling BF_BeginStream, the context will become corrupt if it is passed to any function other than #BF_EncipherStream/#BF_DecipherStream before calling #BF_EndStream.
	@return #BF_RC_SUCCESS				The context record was initialised for stream ciphering successfully.
	@return #BF_RC_INVALID_PARAMETER	The supplied context record pointer is null.
*/
BF_RC BF_BeginStream ( BF_Context* context );

/**
	Clear sensitive data from a context record after performing stream based enciphering/deciphering.
	@param context	Pointer to an initialised context record.
	@remarks The context record may be re-used after this call without needing to call #BF_Init again.
	@remarks After calling BF_EndStream, the context record may be used in non-stream based functions without risking corruption. (See #BF_BeginStream remarks)

	@return #BF_RC_SUCCESS				Sensitive data was cleared from the context record successfully.
	@return #BF_RC_INVALID_PARAMETER	The supplied context record pointer is null.
  */
BF_RC BF_EndStream ( BF_Context* context );

BF_RC BF_EncipherBuffer ( BF_Context* context, const unsigned char *  plainText, unsigned char *  encodedText, unsigned long len );

BF_RC BF_DecipherBuffer ( BF_Context* context, const unsigned char *  encodedText, unsigned char *  plainText, unsigned long len );

#endif /* __BF_H__ */
