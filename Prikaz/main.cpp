#include <exception>
#include <string>
#include <iostream>
#include <fstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include <SDL2/SDL_image.h>

#include "blowfish.h"

#define PASSWORD "PASSWORD_KEY"

#define CAM_IP "169.254.229.254"
#define CAM_PORT 7

/** Converts network order to host order */
int ntohl(int value);

/** Calculates first multiple of 8 larger than given number */
int rounded(int value) { return value % 8 == 0 ? value : value + (8 - (value % 8)); }

/** Read request length of data from socket
    reads until len number of bytes is received
*/
int read_from_socket(TCPsocket socket, unsigned char *data, int len);

/** Decript image */
void decript_image(unsigned char *encripted, unsigned char *decripted, int len);

/** Fetches image from camera */
SDL_Texture *fetch_image(SDL_Renderer *renderer, bool test);

int main(int argc, char *argv[]) {
    //Initializes SDL2
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER)) {
        std::cout << "Failed initializing SDL: " << SDL_GetError() << std::endl;
        return 1;
    }
    //Initializes SDL2 image
    if(IMG_Init(IMG_INIT_JPG) != IMG_INIT_JPG) {
        std::cout << "Failed initializing SDL_Image: " << IMG_GetError() << std::endl;
        return 2;
    }
    //Initializes SDL2 network
    if(SDLNet_Init()) {
        std::cout << "Failed initializing SDLNet: " << SDLNet_GetError() << std::endl;
        return 3;
    }


    // Creates and initializes window
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Texture *texture;

    SDL_CreateWindowAndRenderer(640, 480, SDL_WINDOW_SHOWN, &window, &renderer);
    SDL_SetWindowTitle(window, "LRI");

    // Render empty screen
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    //display camera options
    std::cout<<"t-test image, i-image"<<std::endl;

    SDL_Event event;
    bool running = true;
    while(running) {
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT) {
                running = false;
            } else if(event.type == SDL_KEYDOWN) {
                // Reload image
                SDL_RenderClear(renderer);
                SDL_RenderPresent(renderer);

                // if key pressed is 't' loads test bars
                bool test = false;
                if(event.key.keysym.sym == SDLK_t) {
                    test = true;
                }
                SDL_Texture *temp;
                if(!(temp = fetch_image(renderer, test))) {
                    std::cout << "There was an error while fetching image!" << std::endl;
                } else {
                    std::cout << "Successfully loaded" << std::endl;
                }

                std::cout<<"t-test image, i-image"<<std::endl;
                if(temp) {
                    if(texture) {
                        SDL_DestroyTexture(texture);
                    }
                    texture = temp;
                }
            }
        }

        // Refresh image presented
        SDL_RenderClear(renderer);
        SDL_RenderCopyEx(renderer, texture, NULL, NULL, 0, NULL, SDL_FLIP_VERTICAL);
        SDL_RenderPresent(renderer);

        SDL_Delay(32);
    }

    //Called on window closing
    SDLNet_Quit();
    IMG_Quit();
    SDL_Quit();

    return 0;
}


int ntohl(int value) {
    unsigned char *head = (unsigned char*)&value;
    int ret = 0;
    for(int i=0; i<4; i++) {
        ret <<= 8; ret += (unsigned int)head[i];
    }
    return ret;
}

int read_from_socket(TCPsocket socket, unsigned char *data, int len) {

    std::cout<<"Loading image..."<<std::endl;

    unsigned char *head = data;
    int remaining = len;

    //because image is sent in packages of 1024 bytes at most, image is sent in multiple packages
    //and must be read until all data is loaded
    while(remaining > 0) {
        int read = SDLNet_TCP_Recv(socket, head, remaining);
        remaining -= read;
        head += read;
    }
    return len;
}

void decript_image(unsigned char *encripted, unsigned char *decripted, int len) {
    BF_Context* context = (BF_Context*)malloc(sizeof(BF_Context));
    BF_Init(context, (unsigned char*)PASSWORD, 12, 0x12331432, 0x12314321);
    BF_DecipherBuffer(context, encripted, decripted, rounded(len));
    free(context);
}


SDL_Texture *fetch_image(SDL_Renderer *renderer, bool test) {
    std::cout << "Connecting to camera..." << std::endl;
    IPaddress address;
    SDLNet_ResolveHost(&address, CAM_IP, CAM_PORT);

    TCPsocket socket = SDLNet_TCP_Open(&address);

    if(socket == NULL) {
        return NULL;
    }

    // Sending camera info which image is required
    if(test) {
        SDLNet_TCP_Send(socket, "test", 4);
    } else {
        SDLNet_TCP_Send(socket, "imag", 4);
    }

    // size of image
    int len;
    SDLNet_TCP_Recv(socket, &len, 4);
    len = ntohl(len);

    // buffer used to read encripted image from camera
    unsigned char *encripted_image = new unsigned char[rounded(len)];

    // number of bytes read, it is always same as rounded(len)
    int read = read_from_socket(socket, encripted_image, rounded(len));

    // buffer used to store decripted image
    unsigned char *image = new unsigned char[rounded(len)];

    decript_image(encripted_image, image, len);

    // load texture from image buffer
    SDL_RWops *ops = SDL_RWFromMem(image, len);
    SDL_Texture *texture = IMG_LoadTexture_RW(renderer, ops, 0);

    // release all buffers
    free(encripted_image);
    free(image);

    return texture;
}
