#ifndef _CAMERA_H_
#define _CAMERA_H_

/**
 * Initializes camera for reading images
 * */
int camera_init_iic();

/**
 * Load image from camera
 * @param test_mode if 1 loads test bar otherwise loads image from camera
 * */
void camera_load_image(int test_mode);

#endif
