#include "image.h"

#include "encripter/blowfish.h"

static unsigned char *encoded_image = NULL;
static int encoded_size = 0;
static int image_size = 0;

struct _image *get_image() {
	struct _image *image = (struct _image*)malloc(sizeof(struct _image));
	image->image = encoded_image;
	image->image_size = image_size;
	image->remaining = encoded_size;
	image->started = 0;
	return image;
}

// return first larger multiple of 8
int get_buffer_size(int size) {
	return size % 8 == 0 ? size : (size + (8 - (size % 8)));
}

void set_image(unsigned char *buffer, int len) {
	unsigned char *input_buffer = (unsigned char*)malloc(get_buffer_size(len));
	if(input_buffer == NULL) {
		printf("Failed allocation input buffer for encoding");
	}
	if(encoded_image) {
		free(encoded_image);
	}
	encoded_image = malloc(get_buffer_size(len));
	if(encoded_image == NULL) {
		printf("Failed allocation buffer for encoding");
	}

	encoded_size = get_buffer_size(len);
	image_size = len;

	memcpy(input_buffer, buffer, len);

	BF_Context* context = (BF_Context*)malloc(sizeof(BF_Context));
	BF_Init(context, "PASSWORD_KEY", 12, 0x12331432, 0x12314321);
	BF_EncipherBuffer(context, input_buffer, encoded_image, encoded_size);

	free(input_buffer);
	free(context);
}

