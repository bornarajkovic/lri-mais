#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//image structure used for sending
struct _image {
	//encripted image
	char *image;
	//because blowfish works on blocks of 8 bytes
	//image_size and size of image buffer can be different
	int image_size;
	//size of image buffer
	int remaining;
	//was image sending already started
	int started;
};

/**
 * Returns image struct
 * **THIS POINTER NEEDS TO BE FREED using free(image)**
 * */
struct _image *get_image();

/**
 * Prepares image for sending
 *
 * @param buffer - jpeg image
 * @param len - size of jpeg
 * */
void set_image(unsigned char *buffer, int len);

#endif
