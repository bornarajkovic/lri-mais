# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\Users\borna\workspace\neti_design_wrapper\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\Users\borna\workspace\neti_design_wrapper\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {neti_design_wrapper}\
-hw {C:\Users\borna\Documents\FER7\mais\lri_01_12\lri_01_12\neti_design_wrapper.xsa}\
-fsbl-target {psu_cortexa53_0} -out {C:/Users/borna/workspace}

platform write
domain create -name {standalone_ps7_cortexa9_0} -display-name {standalone_ps7_cortexa9_0} -os {standalone} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {lwip_echo_server}
platform generate -domains 
platform active {neti_design_wrapper}
domain active {zynq_fsbl}
domain active {standalone_ps7_cortexa9_0}
platform generate -quick
platform generate
bsp reload
domain active {zynq_fsbl}
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp config phy_link_speed "CONFIG_LINKSPEED10"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains standalone_ps7_cortexa9_0 
bsp config phy_link_speed "CONFIG_LINKSPEED_AUTODETECT"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains standalone_ps7_cortexa9_0 
platform active {neti_design_wrapper}
domain active {zynq_fsbl}
bsp reload
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp config tcp_snd_buf "65536"
bsp write
bsp reload
catch {bsp regenerate}
platform clean
platform clean
platform active {neti_design_wrapper}
domain active {zynq_fsbl}
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp reload
bsp write
bsp config tcp_snd_buf "8192"
bsp write
bsp reload
catch {bsp regenerate}
platform generate
platform active {neti_design_wrapper}
platform generate -domains 
platform clean
platform generate
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform active {neti_design_wrapper}
platform clean
platform generate
bsp reload
bsp write
platform generate -domains 
platform active {neti_design_wrapper}
domain active {zynq_fsbl}
bsp reload
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp reload
platform config -updatehw {C:/Users/borna/Documents/FER7/mais/lri_01_12/lri_01_12/neti_design_wrapper.xsa}
bsp reload
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
bsp reload
domain active {zynq_fsbl}
bsp reload
platform generate -domains 
platform clean
