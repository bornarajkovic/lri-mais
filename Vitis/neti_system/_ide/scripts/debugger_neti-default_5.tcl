# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: C:\Users\borna\workspace\neti_system\_ide\scripts\debugger_neti-default_5.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source C:\Users\borna\workspace\neti_system\_ide\scripts\debugger_neti-default_5.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Xilinx PYNQ-Z1 003017A4D017A" && level==0 && jtag_device_ctx=="jsn-Xilinx PYNQ-Z1-003017A4D017A-23727093-0"}
fpga -file C:/Users/borna/workspace/neti/_ide/bitstream/neti_design_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw C:/Users/borna/workspace/neti_design_wrapper/export/neti_design_wrapper/hw/neti_design_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source C:/Users/borna/workspace/neti/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow C:/Users/borna/workspace/neti/Debug/neti.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con
